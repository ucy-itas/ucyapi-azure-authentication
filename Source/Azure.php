<?php
namespace Iss\Api\Service\AzureAuthentication\Source;

use Iss\Api\{ChainOfResponsibilityInterface,
    InjectableTrait,
    ChainOfResponsibilityTrait,
    Service\AzureAuthentication\Constraint\PermittedForAnyOf};
use Iss\Api\Service\AzureAuthentication\Authentication;
use Iss\Api\Authentication\Principal;
use Iss\Api\Handler;
use Lcobucci\Clock\SystemClock;
use Phalcon\Config\Config;
use Phalcon\Di\InjectionAwareInterface;
use Iss\Api\Messaging\{Request, Response\Error};
use Firebase\JWT\JWK;
use Lcobucci\JWT\{Encoding\JoseEncoder, Token\Parser, Validation\Validator};
use Lcobucci\JWT\Validation\Constraint\{IssuedBy, PermittedFor, SignedWith, LooseValidAt};
use Lcobucci\JWT\Signer\{Key\InMemory, Rsa\Sha256};
use Iss\Api\Service\AzureAuthentication\Constraint\None;
use function Iss\Api\Service\AzureAuthentication\is_jwt;

class Azure implements ChainOfResponsibilityInterface
{
    use ChainOfResponsibilityTrait;
    use InjectableTrait;

    private bool $enabled = true;

    protected function process(array $request): Principal|Error|null
    {
        [$method, $token] = $request;
        return $this->authenticate($method, $token);
    }

    public function authenticate($method, $token): Principal|Error|null
    {
        if (!$this->isEnabled()) {
            return null;
        }

        if ($method !== 'Bearer') {
            return null;
        }

        if (!is_jwt($token)) {
            return null;
        }
        try {
            $token = (new Parser(new JoseEncoder()))->parse((string)$token); // Parses from a string
        } catch (\Exception $exception) {
            return null;
        }

        if (!$token->claims()->has('iss') || !$token->claims()->has('ver')) {
            // Probably JWT from other source. leave it to another class
            return null;
        }

        $config = $this->getDI()->getShared('config')->get(
            'service/' . Authentication::getName(), __DIR__ . '/../config'
        )->merge(
            $this->getDI()->getShared('config')->get('service/' . Authentication::getName())
        );

        $token_version = $token->claims()->get('ver');

        //need to change the delimiter because token_version contains '.'
        $iss = $config->path("iss:$token_version", '', ':');

        $directory_id = $config->get('directory-id', '');
        $iss = str_replace('{directory-id}', $directory_id, $iss);

        if ($token->claims()->get('iss') !== $iss) {
            // Probably JWT from other source. leave it to another class
            return null;
        }

        $aud = $config->path("aud:$token_version", new Config(), ':');
        if ($aud instanceof Config) {
            $aud = $aud->toArray();
        } elseif (is_scalar($aud)) {
            $aud = [$aud];
        }
        if ($aud) {
            $audienceConstraint = new PermittedForAnyOf($aud);
        } else {
            $audienceConstraint = new None();
        }

        $validator = new Validator();
        $issuerConstraint = new IssuedBy($iss);
        $valid_at = new LooseValidAt(SystemClock::fromUTC(), new \DateInterval("PT60S"));

        $valid = true;
        try {
            $validator->assert($token, $issuerConstraint, $audienceConstraint, $valid_at);
        } catch (\Exception $e) {
            $valid = false;
        }

        if (!$valid) {
            return new Error\Unauthorized('Token invalid');
        }

        $kid = $token->headers()->get('kid', '');
        if (!$kid) {
            return new Error\Unauthorized('Token invalid');
        }

        $cache = $this->getDI()->get('cache');

        //Even if redis does not exist, there always is a default cache
        $cache = $cache->getCache('redis');

        $well_known_path = $iss . '/.well-known/openid-configuration';
        $well_known_hash = md5($well_known_path);
        $contents = $cache->get($well_known_hash);
        if (!$contents) {
            $contents = file_get_contents($well_known_path);
            $cache->set($well_known_hash, $contents);
        }

        $ms_config = json_decode($contents);

        if (!isset($ms_config->jwks_uri)) {
            return new Error\Unauthorized('Token cannot be verified');
        }

        $keys_uri  = $ms_config->jwks_uri;
        $keys_uri_hash = md5($keys_uri);
        $keys = $cache->get($keys_uri_hash);
        if (!$keys) {
            $keys = file_get_contents($keys_uri);
            $cache->set($keys_uri_hash, $keys);
        }

        $ms_keys = json_decode($keys, true);

        if (!$ms_keys) {
            return new Error\Unauthorized('Token cannot be verified');
        }

        $keys = JWK::parseKeySet($ms_keys, 'RS256');
        if (!isset($keys[$kid])) {
            return new Error\Unauthorized('Token cannot be verified');
        }

        $key = InMemory::plainText(openssl_pkey_get_details ($keys[$kid]->getKeyMaterial())['key']);

        $signer = new Sha256();
        try {
            $verified = $validator->validate($token, new SignedWith($signer, $key));
        } catch (\Exception $e) {
            $verified = false;
        }

        if (!$verified) {
            return new Error\Unauthorized('Token cannot be verified');
        }

        if ($token->claims()->has('onprem_sid')) {
            $ad_sid = $token->claims()->get('onprem_sid');
        } else {
            return new Error\Unauthorized('Token invalid, user id not found');
        }

        $handler_config = $this->getDI()->getShared('config')->get('handler/aduser');
        $result = (new Handler($handler_config))->getResource(new Request(['sid' => $ad_sid, 'return' => ['ucy_id', 'username']]));

        if ($result instanceof Error) { // User not found
            return new Error\Unauthorized('User not found');
        }

        $authentication_request = new Request([
            'name' => strtoupper($result['username']),
            'ucy_id' => $result['ucy_id'],
            'return' => ['name', 'ucy_id', 'id']
        ]);

        $authentication_handler = new Handler($this->getDI()->getShared('config')->get('handler/authentication/client'));
        $client_result = $authentication_handler->getResource($authentication_request);

        if ($client_result instanceof Error) { // Client not found attempt to insert client
            if ($config->get('create-user', false)) {
                $client_request = new Request();
                $client_request_data = new \stdClass();
                $client_request_data->data = new \stdClass();
                $client_request_data->data->attributes = new \stdClass();
                $client_request_data->data->attributes->name = strtoupper($result['username']);
                $client_request_data->data->attributes->ucy_id = $result['ucy_id'];
                $client_request_data->data->attributes->type = $config->path('user-defaults.type', 'USER');
                $client_request_data->data->attributes->key = $config->path('user-defaults.key', 'SSO');
                $client_request_data->data->attributes->vendor = $config->path('user-defaults.vendor', 'AZURE');
                $client_request->setData($client_request_data);
                $authentication_handler->post($client_request);
                $client_result = $authentication_handler->getResource($authentication_request);
            }
            if ($client_result instanceof Error) { // Client not found
                return new Error\Unauthorized('User not found');
            }
        }
        $provider = $token->claims()->get('aud');
        if (is_array($provider)) {
            $provider = $provider[0];
        }
        $client_result['provider'] = $provider;

        return new Principal($client_result);
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }
}