<?php
namespace Iss\Api\Service\AzureAuthentication;

use Iss\Api\{
    ServiceInterface,
    ServiceTrait};
use Iss\Api\Service\AzureAuthentication\Source\Azure;
use Phalcon\Mvc\Micro;

class Authentication implements ServiceInterface
{
    use ServiceTrait;

    private Azure $authenticator;

    public function register(Micro $application): bool
    {
        if (!$application->getDI()->has('authentication')) {
            // authorization service is not ready yet
            return false;
        }
        $this->authenticator = new Azure();
        $this->authenticator->setEnabled(true);
        $application['authentication']->appendSuccessor($this->authenticator);
        return true;
    }

    public function unregister(Micro $application): ?ServiceInterface
    {
        $this->authenticator->setEnabled(false);
        return $this;
    }

    public static function getName(): string
    {
        return 'azure-authentication';
    }
}
