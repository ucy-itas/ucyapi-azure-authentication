<?php
namespace Iss\Api\Service\AzureAuthentication;

function is_jwt($value): bool
{
    if (is_string($value)) {
        if (preg_match('/^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+=]*$/', $value) === 1) {
            return true;
        }
    }
    return false;
}