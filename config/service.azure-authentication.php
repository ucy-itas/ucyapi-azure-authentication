<?php


return [
    'directory-id' => '',
    'aud' => [],
    'iss' => [
        '1.0' => 'https://sts.windows.net/{directory-id}/',
        '2.0' => 'https://login.microsoftonline.com/{directory-id}/v2.0'
    ],
    'create-user' => true,
    'user-defaults' => [
        'key' => 'SSO',
        'vendor' => 'AZURE',
        'type' => 'USER'
    ]
];
