<?php

namespace Iss\Api\Service\AzureAuthentication\Constraint;

use Lcobucci\JWT\Token;
use Lcobucci\JWT\Validation\{Constraint, ConstraintViolation};

class PermittedForAnyOf implements Constraint
{
    /** @param string[] $audience */
    public function __construct(private readonly array $audience)
    {
    }

    /**
     * @inheritDoc
     */
    public function assert(Token $token): void
    {
        foreach ($this->audience as $audience) {
            if ($token->isPermittedFor($audience)) {
                return;
            }
        }
        throw ConstraintViolation::error(
            'The token is not allowed to be used by this audience',
            $this,
        );
    }
}
